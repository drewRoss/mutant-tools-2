import session from 'appkit/session';
export default Ember.View.extend({

	templateName: "login",

	didInsertElement: function() { 

		// Disable Navbar Login while on Login Page
		session.set('onLogin',true);
		
	}, 

	focusIn: function(){

		// Clear the error message when trying again
		this.set('controller.serverErrorMessage', '');

	}


});