var Router = Ember.Router.extend(); // ensure we don't share routes between all Router instances

Router.map(function() {

	this.route('component-test');
	this.route('helper-test');

	this.route('dynform');

	this.route('home');
	this.route('login');
	this.route('welcome');

	this.resource("account", function () {

		this.resource("account.details", {path: '/details'}, function () {
			this.route("index", { path: "/" });
		});
		this.resource("account.history", {path: '/history'}, function () {
			this.route("index", { path: "/" });
		});
	
		this.resource("account.password", {path: '/password'}, function () {
			this.route("index", { path: "/" });
			this.route("set", { path: "set/:reset_token" });
			this.route("reset", { path: "reset" });
		});

		this.resource("account.register", {path: '/register'}, function () {
			this.resource("account.register.confirmed", {path: '/confirmed'}, function () {
				this.route("index", { path: "/" });
			});
		});

	});

	this.resource("manage", function () {
		this.resource("manage.website", {path: '/website'}, function () {
			this.route("index", { path: "/" });
			this.route('edit', { path: "edit/:edit_id" });
		});
		this.resource("manage.account", {path: '/account'}, function () {
			this.route("index", { path: "/" });
			this.route('edit', { path: "edit/:edit_id" });
		});
		this.resource("manage.resource", {path: '/resource'}, function () {
			this.route("index", { path: "/" });
			this.route('edit', { path: "edit/:edit_id" });
		});
		this.resource("manage.agency", {path: '/agency'}, function () {
			this.route("index", { path: "/" });
			this.route('edit', { path: "edit/:edit_id" });
		});
		this.resource("manage.place", {path: '/place'}, function () {
			this.route("index", { path: "/" });
			this.route('edit', { path: "edit/:edit_id" });
		});
		this.resource("manage.registered-agent", {path: '/registered-agent'}, function () {
			this.route("index", { path: "/" });
			this.route('edit', { path: "edit/:edit_id" });
		});
		this.resource("manage.product", {path: '/product'}, function () {
			this.route("index", { path: "/" });
			this.route('edit', { path: "edit/:edit_id" });
		});
	});
	
	this.resource("client", function () {
		this.resource("client.company", {path: '/company'}, function () {
			this.route("index", { path: "/" });
			this.route('edit', { path: "edit/:edit_id" });
			this.route('add');
		});
	});

});

export default Router;