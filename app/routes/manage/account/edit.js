export default Ember.Route.extend({
  model: function (params) {
    return $.getJSON("/accounts").then(function (dat) {
			Ember.Logger.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~ ROUTES -> MANAGE -> ACCOUNT -> MODEL -> GETDAT");
      Ember.Logger.log(params.edit_id);
      Ember.Logger.log(dat);
      return new Ember.RSVP.hash({
	      model: Ember.Object.create(Ember.A(dat.accounts).findBy('primary_email', params.edit_id)),
				id: Ember.computed.alias('model._id'),
	      params: params
      });
    });
  },
  setupController: function (controller, model) {
	  controller.set('model', model.model);
	  controller.set('id', model.params.edit_id);
		if(model.params.edit_id==='new'){
			controller.set('newAccount', 'new');
			//controller.set('id', '');
			controller.set('name', '');
			// TODO: This clears id for new account, but placeholders still hidden for name, company etc.
		}
  }
});

//id: Ember.computed.alias('model._id')