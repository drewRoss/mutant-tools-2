export default Ember.Route.extend({
	model: function() {
		var datObj = [];
		$.getJSON("/accounts").then(function(dat) { ///assets/json/accounts.json
			dat.accounts.forEach(function(data) {
				datObj.pushObject(data);
			});
		});
		return datObj;
	},
	actions: {
		addNew: function () {
			Ember.Logger.log("~~~~~~ CONTROLLER -> MANAGE -> ACCOUNT -> ADD NEW ");
			this.transitionTo('manage.account.edit', 'new');
		},
		deleteItem : function (del_id){
			var data = {};
					data.id = del_id;
			Ember.Logger.log("~~~~~~ CONTROLLER -> MANAGE -> ACCOUNT -> EDIT : DELETING ITEM - ",del_id);
			Ember.$.post('/accounts/delete', data, function(response) {
				if (response.success) {
					Ember.Logger.log("~~~~~~ CONTROLLER -> MANAGE -> ACCOUNT -> DELETE : SUCCESS");
					this.refresh();
				} else {
					Ember.Logger.log("~~~~~~ CONTROLLER -> MANAGE -> ACCOUNT -> DELETE : ERROR");
					this.refresh();
				}
			}.bind(this), 'json');
		}
	}
});