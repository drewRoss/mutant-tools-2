export default Ember.Route.extend({
	model: function() {
		var datObj = [];
		$.getJSON("/assets/json/agencies.json").then(function(dat) {
			dat.agencies.forEach(function(data) {
				datObj.pushObject(data);
			});
		});
		return datObj;
	},
	actions: {
		addNew: function () {
			this.transitionTo('manage.agency.edit');
		}
	}
});