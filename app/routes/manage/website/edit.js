export default Ember.Route.extend({
	model: function(params) {
		
	},

	actions: {
		uploadLogoModal: function(uploadedImg) {
				this.controllerFor('manage/website/edit').set('uploadedImg', uploadedImg);
				this.render('modals/image-preview', { into: 'application', outlet: 'modal', controller: 'manage/website/edit', view: 'modal' });
		},
		close: function() {
				this.disconnectOutlet({outlet: 'modal', parentView: 'application'});
		}
	}
});


