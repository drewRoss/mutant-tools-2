export default Ember.Route.extend({
	model: function() {
		var datObj = [];
		$.getJSON("/assets/json/websites.json").then(function(dat) {
			dat.websites.forEach(function(data) {
				datObj.pushObject(data);
			});
		});
		return datObj;
	},
	actions: {
		addNew: function () {
			this.transitionTo('manage.website.edit');
		}
	}
});