export default Ember.Route.extend({
	model: function() {
		var datObj = [];
		$.getJSON("/assets/json/resources.json").then(function(dat) {
			dat.resources.forEach(function(data) {
				datObj.pushObject(data);
			});
		});
		return datObj;
	},
	actions: {
		addNew: function () {
			this.transitionTo('manage.resource.edit');
		}
	}
});