export default Ember.Route.extend({
	model: function() {
		var datObj = [];
		$.getJSON("/assets/json/places.json").then(function(dat) {
			dat.places.forEach(function(data) {
				datObj.pushObject(data);
			});
		});
		return datObj;
	},
	actions: {
		addNew: function () {
			this.transitionTo('manage.place.edit');
		}
	}
});