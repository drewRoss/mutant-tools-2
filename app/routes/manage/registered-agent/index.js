export default Ember.Route.extend({
	model: function() {
		var datObj = [];
		$.getJSON("/assets/json/registeredagents.json").then(function(dat) {
			dat.registeredagents.forEach(function(data) {
				datObj.pushObject(data);
			});
		});
		return datObj;
	},
	actions: {
		addNew: function () {
			this.transitionTo('manage.registered-agents.edit');
		}
	}
});