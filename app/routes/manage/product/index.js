export default Ember.Route.extend({
	model: function() {
		var datObj = [];
		$.getJSON("/assets/json/products.json").then(function(dat) {
			dat.products.forEach(function(data) {
				datObj.pushObject(data);
			});
		});
		return datObj;
	},
	actions: {
		addNew: function () {
			this.transitionTo('manage.product.edit');
		}
	}
});