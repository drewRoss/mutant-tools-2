import session from 'appkit/session';
export default Ember.Route.extend({
	setupController: function(controller, model) {
		Ember.Logger.log("~~~~~~~~~~~~~~~ ROUTES -> LOGIN ~~~~~~~~~~~~~~~");
	},
	actions: {
		willTransition: function(transition) {
			Ember.Logger.log('~~~~~~~~~ TRANSITIONED AWAY FROM LOGIN PAGE : ONLOGIN FALSE ~~~~~~~~~~');
			session.set('onLogin',false);
		}
	}
});