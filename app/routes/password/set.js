export default Ember.Route.extend({

	model: function(params) {
		Ember.Logger.log("~~~~~~~~~~~~~~~ ROUTES -> PASSWORD -> SET ~~~~~~~~~~~~~~~", params.set_id);
		return {'set_id': params.set_id};
	}

});