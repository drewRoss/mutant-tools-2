import session from 'appkit/session';
export default Ember.Route.extend({

	session: session,

	actions: {

		login: function(from) {

			var rememberMe = this.controllerFor(from).get('rememberMe');
			if(rememberMe===true){ rememberMe = 604800; }

			var data = {
					'email':this.controllerFor(from).get('email'), 
					'password':this.controllerFor(from).get('password'),
					'rememberMe':rememberMe
				};

			Ember.Logger.log('~~~~~~~~~ APP -> ROUTES -> APPLICATION -> LOGIN : (data):', data);
			this.set('errorMessage', null); // reset error messages
			Ember.$.post('/accounts/login', data, function(response) { //accounts/loginaccounts_login_ok

				if (response.success === true) {

					session.set('isLoggedIn', true);
					session.set('loginToken', response.token);
					session.set('loginRoles', response.account.roles);
					session.set('loginLast', moment(response.account.last_login).format('MMMM Do YYYY, h:mm:ss a'));
					session.set('loginLastIP', response.account.last_ip);
					this.send('setSessionCookie', response.login_detail.account_id, response.token, response.account.roles, from);
					this.send('getSessionCookie');
					Ember.Logger.log('~~~~~~~~~ APP -> ROUTES -> APPLICATION -> LOGIN : SUCCESS (isLoggedIn, loginToken, loginRoles, loginLast, loginLastIP):', session.get('isLoggedIn'), session.get('loginToken'), session.get('loginRoles'), session.get('loginLast'), session.get('loginLastIP'));
					this.transitionTo('home');

				} else {

					var errorMessage = 'login not accepted, please try again. Error Code: ' + response.errors;
					Ember.Logger.log('~~~~~~~~~ APP -> ROUTES -> APPLICATION -> LOGIN : ERROR : serverErrorMessage : ', errorMessage );
					this.controllerFor(from).set('serverErrorMessage', errorMessage);
					this.controllerFor(from).set('password', '');

				}

			}.bind(this), 'json');

		},

		setSessionCookie: function(id, token, roles, from) {

			Ember.Logger.log('~~~~~~~~~ APP -> ROUTES -> APPLICATION -> SET SESSION COOKIE :', id, ', TOKEN:', token, ' ROLES:', roles, ' FROM:', from);
			var rememberMe = this.controllerFor(from).get('rememberMe');
			if(rememberMe===true){
				$.cookie('corptools_session_auth_token', token, { path: '/', expires : 7  });
				$.cookie('corptools_session_auth_id', id, { path: '/', expires : 7  });
				$.cookie('corptools_session_auth_roles', roles, { path: '/', expires : 7  });
			}	else {
				$.cookie('corptools_session_auth_token', token, { path: '/' });
				$.cookie('corptools_session_auth_id', id, { path: '/' });
				$.cookie('corptools_session_auth_roles', roles, { path: '/' });
			}
		
		},

		getSessionCookie: function () {

			var authToken = $.cookie('corptools_session_auth_token'),
					authIdent = $.cookie('corptools_session_auth_id'),
					authRoles = $.cookie('corptools_session_auth_roles');
			
			var rolesArr = authRoles ? authRoles.split(/,/) : Em.A();

			Ember.Logger.log('~~~~~~~~~ APP -> ROUTES -> APPLICATION -> GET SESSION COOKIE :', authToken, ', authIdent:', authIdent, ' authRoles:', rolesArr);

			if (Em.isEmpty(authToken)){

				Ember.Logger.log('~~~~~~~~~ APP -> ROUTES -> APPLICATION -> GET SESSION COOKIE : NO TOKEN :');
				session.set('isLoggedIn', false);
				session.set('loginToken', false);
				session.set('loginRoles', Em.A());
				
			} else {

				Ember.Logger.log('~~~~~~~~~ APP -> ROUTES -> APPLICATION -> GET SESSION COOKIE : TOKEN :', authToken, rolesArr);
				session.set('isLoggedIn', true);
				session.set('loginToken', authToken);
				session.set('loginRoles', rolesArr);

				$.ajaxSetup({
					headers: { 'X-Auth-Header': authToken }
				});

				Ember.Logger.log('~~~~~~~~~ APP -> ROUTES -> APPLICATION -> GET SESSION COOKIE : THIS IS YOUR CURRENT SESSION OBJECT  :', session);


				//TODO: YOU SHOULD PROBABLY CHECK TO MAKE SURE THAT authToken IS A VALID NON-EXPIRED AUTH TOKEN


			}

		},

	},

	beforeModel: function (transition) {

		Ember.Logger.log('~~~~~~~~~ APP -> ROUTES -> APPLICATION -> BEFOREMODEL : TRANSITION(getSessionCookie) :', transition);
		transition.send('getSessionCookie');

	}

});
