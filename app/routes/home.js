import session from 'appkit/session';
export default Ember.Route.extend({

	/*	
			TODO: create an authenticated route and extend your other routes from it
			So instead of all your routes extending Ember.Route all your non-login 
			routes will extend your new AuthenticatedRoute that extends Ember.Route
	*/

	afterModel: function(){
		if(!session.get('isLoggedIn')){
			Ember.Logger.log('~~~~~~~~~ NOT LOGGED IN ~~~~~~~~~');
			return this.replaceWith('welcome');
		}
	}	

});