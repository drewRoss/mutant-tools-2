import statics from 'appkit/statics';

var controls = Ember.Object.create({

	"dynform_test" : [

			{

				"id" : "1", 
				"label" : "email",
				"type" : "text",
				"name" : "email",
				"form-id" : "input1",
				"class" : "form-control",
				"value" : "",
				"placeholder" : "email@email.com",
				"validation" : "presence"

			},

			{

				"id" : "2", 
				"label" : "confirm password",
				"type" : "password",
				"name" : "passwordConfirmation",
				"form-id" : "input2",
				"class" : "form-control",
				"value" : "",
				"placeholder" : "form two placeholder",
				"validation" : "presence"

			},		

			{

				"id" : "3",
				"label" : "big text",
				"isTextarea" : true,
				"name" : "input3",
				"form-id" : "input3",
				"class" : "form-control",
				"cols" : "4",
				"value" : "",
				"placeholder" : "textarea placeholder",
				"validation" : "presence"

			},

			{

				"id" : "4",
				"label" : "drop down options",
				"isSelect" : true,
				"name" : "input4",
				"form-id" : "input4",
				"class" : "form-control",
				"options" : [
					{ "val": "1", "label": "one" },
					{ "val": "2", "label": "two" },
					{ "val": "3", "label": "three"},
					{ "val": "4", "label": "four" },
				],
				"selectedValue" : "3",
				"validation" : "presence"

			},

			{

				"id" : "5",
				"isSelect" : true,
				"label" : "drop down object",
				"name" : "input5",
				"form-id" : "input5",
				"class" : "form-control",
				"options" : statics.us_states,
				"selectedValue" : "AL",
				"validation" : "presence"

			},

		]

});

export default controls;
