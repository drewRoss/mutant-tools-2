import session from 'appkit/session';
export default Ember.ArrayController.extend(Ember.Validations.Mixin, {
	
	validations: {
		email: {
			presence: { message: " email required" },
			format: { with: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/ , message: " example: me@email.com" }
		},
		password: {
			presence: { message: " password required" }
		}
	},

	session: session,
	isLoggedIn: Ember.computed.alias('session.isLoggedIn'),
	isOnLoginPage: Ember.computed.alias('session.onLogin'),
	loginRoles: Ember.computed.alias('session.loginRoles'),
	isFreeAccount: false,
	isClient: false,
	isAdmin: false,
	isAffiliate: false,
	isLocalOffice: false,
	isDisabled: false,
	isSuspended: false,

	actions: {

		clearLoginDrop: function(){
			Ember.Logger.log('~~~~~~~~~ CLEAR LOGIN DROP DOWN ~~~~~~~~~');
			this.set('email', '');
			this.set('password', '');
			this.set('serverErrorMessage', '');
		},
		
		logout: function() {

			this.set('email', false);
			this.set('password', false);
			this.controllerFor('login').set('email', '');
			this.controllerFor('login').set('password', '');
			session.set('isLoggedIn', false);
			session.set('loginToken', false);
			$.removeCookie('corptools_session_auth_token');
			$.removeCookie('corptools_session_auth_id');
			$.removeCookie('corptools_session_auth_roles');
			this.transitionToRoute('welcome');

			Ember.Logger.log('~~~~~~~~~ LOGOUT (isLoggedIn,loginToken):', session.get('isLoggedIn'), session.get('loginToken'));

		}

	},

	doLoginRoles: function() {

		Ember.Logger.log('~~~~~~~~~ APP -> CONTROLLERS -> NAV -> doLoginRoles -> CALLED~~~~~~~~~', typeof(session.get('loginRoles')));
		Ember.Logger.log(session.get('loginRoles'));

		session.get('loginRoles').forEach(function(role) {

			if (role === 'free-account') { this.set('isFreeAccount',true); }
			if (role === 'client') { this.set('isClient',true); }
			if (role === 'admin') { this.set('isAdmin',true); }
			if (role === 'affiliate') { this.set('isAffiliate',true); }
			if (role === 'local-office') { this.set('isLocalOffice',true); }
			if (role === 'disabled') { this.set('isDisabled',true); }
			if (role === 'suspended') { this.set('isSuspended',true); }

		}.bind(this));

			Ember.Logger.log('~~~~~~~~~ APP -> CONTROLLERS -> NAV -> doLoginRoles -> isADMIN ~~~~~~~~~', this.get('isAdmin'));			


	}.on('init'),

	doLoginRolesObserver: function() {
		
		Ember.Logger.log('~~~~~~~~~ APP -> CONTROLLERS -> NAV -> doLoginRoles -> doOBSERVER  ~~~~~~~~~', this.get('loginRoles'));
		return this.doLoginRoles();
	
	}.observes('loginRoles')

});

// TODO: Make sure these run on refresh... this is not active anymore
// this.send('setSessionCookie', response.login_detail.account_id, response.token, response.account.roles);
// this.send('getSessionCookie');


