import session from 'appkit/session';
export default Ember.Controller.extend(Ember.Validations.Mixin, {
	
	validations: {
		email: {
			presence: { message: " email required" },
			format: { with: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/ , message: " example: me@email.com" }
		},
		password: {
			presence: { message: " password required" }
		}
	},

	session: session,

	rememberMe: true, 

	isLoggedIn: Ember.computed.alias('session.isLoggedIn'),

	clearServerMessageObserver: function (){

		Ember.Logger.log('~~~~~~~~~ CLEAR SERVER MESSAGE OBSERVER ~~~~~~~~~:',this.get('serverErrorMessage'));

	}.observes('serverErrorMessage'),

});