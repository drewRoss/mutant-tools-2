import statics from 'appkit/statics';
export default Ember.ObjectController.extend({
	statics: statics,
	generatePassword: true,
	init: function (){
		Ember.Logger.log('~~~~~~ CONTROLLER -> MANAGE -> ACCOUNT -> EDIT : INIT');
	},
	actions : {
		createNew : function () {
			Ember.Logger.log("~~~~~~ CONTROLLER -> MANAGE -> ACCOUNT -> EDIT : CREATE NEW");
			var data = this.getProperties(
									'primary_email',
									'admin', 'affiliate', 'localoffice', 'client',
									'name', 'company', 'address', 'suite',
									'city', 'state', 'zipcode', 'country',
									'phone', 'fax', 'www'
								);
			Ember.Logger.log(data);
			Ember.$.post('/accounts/create', data, function(response) {
				if (response.success) {
					Ember.Logger.log("~~~~~~ CONTROLLER -> MANAGE -> ACCOUNT -> EDIT : SUCCESS");
					this.transitionToRoute('manage.account');
				}
			}.bind(this), 'json');
		},
		updateItem : function (uid) {
			Ember.Logger.log("~~~~~~ CONTROLLER -> MANAGE -> ACCOUNT -> EDIT : UPDATE ITEM");
			var data = this.getProperties(
									'primary_email', 
									'admin', 'affiliate', 'localoffice', 'client',
									'name', 'company', 'address', 'suite',
									'city', 'state', 'zipcode', 'country',
									'phone', 'fax', 'www'
								);
					data.id = uid;
			Ember.Logger.log(data);
			Ember.$.post('/accounts/update', data, function(response) {
				if (response.success) {
					Ember.Logger.log("~~~~~~ CONTROLLER -> MANAGE -> ACCOUNT -> UPDATE : SUCCESS");
					this.transitionToRoute('manage.account');
				}
			}.bind(this), 'json');
		}
	}
});