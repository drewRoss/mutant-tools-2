import session from 'appkit/session';
import statics from 'appkit/statics';
export default Ember.Controller.extend({
	
	needs: 'nav',
	session: session,
	
	lastLoginMessage: function () {
	
		var loginLastIP = this.get('session.loginLastIP'),
				loginLast = this.get('session.loginLast');
		if ( loginLastIP && loginLast ) {
			return 'Last-login from ip: [' + loginLastIP + '] on [' + loginLast + '].';
		}
	
	}.property('session.loginLastIP', 'session.loginLast'),

});

