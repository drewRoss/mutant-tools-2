import session from 'appkit/session';
import statics from 'appkit/statics';
export default Ember.Controller.extend(Ember.Validations.Mixin, {
	
	validations: {
		password: {
			confirmation: true,
			presence: {
				message: ' password'
			}
		},
		
		passwordConfirmation: {
			presence: {
				message: ' please confirm password'
			}
		}
	},

	generatePassword: true,
	serverErrorMessage: false,
	serverSuccessMessage: false,

	generatePasswordObserver: function (){
		this.validate().then(function() { 
			Ember.Logger.log('~~~~~~~~~ APP -> CONTROLLER -> HOME -> REVALIDATE FOR ACCOUNT PASSWORD'); 
		});
	}.observes('generatePassword'),


	needs: 'nav',
	session: session,
	statics: statics,
	internationalAddress: false,
	settingPassword: false,
	addingNewAddress: false,
	addingNewPhone: false,
	addingNewEmail: false,
	readyToSave: Ember.computed.or('addingNewAddress', 'addingNewPhone', 'addingNewEmail'),
	newAddresses: [],
	newPhones: [],
	newEmails: [],

	lastLoginMessage: function () {
	
		var loginLastIP = this.get('session.loginLastIP'),
				loginLast = this.get('session.loginLast');
		if ( loginLastIP && loginLast ) {
			return 'Last-login from ip: [' + loginLastIP + '] on [' + loginLast + '].';
		}
	
	}.property('session.loginLastIP', 'session.loginLast'),

	internationalAddressObserver: function () {

		if(this.get('internationalAddress')===true){
			this.set('state','');
			this.set('country','CA');
		} else {
			this.set('state','WA');
			this.set('country','US');
		}

	}.observes('internationalAddress'),

	getAccountDetails: function(){

		Ember.$.getJSON('/account/details', function(response) { //account_details_ok
			if (response) {
				Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : GET ACCOUNT DETAILS ',response);
				this.set('first_name', response.first_name);
				this.set('last_name', response.last_name);
				if (response.addresses) {
					response.addresses.forEach(function(address) {
						this.get('newAddresses').addObject(Em.Object.create(address));
					}.bind(this));
				}
				if (response.phone_numbers) {
					response.phone_numbers.forEach(function(number) {
						this.get('newPhones').addObject(Em.Object.create(number));
					}.bind(this));
				}
				if (response.emails) {
					response.emails.forEach(function(email) {
						this.get('newEmails').addObject(Em.Object.create(email));
					}.bind(this));
				}
			}
		}.bind(this), 'json');

	}.on('init'),
	
	actions : {

		/* Set Password */

		setPassword: function (){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : SET PASSWORD FORM ');
			this.set('settingPassword', true);

		},

		cancelPassword: function (){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : CANCEL PASSWORD FORM ');
			this.setProperties({
				'password'				:	'',
				'confirmPassword'	: ''							
			});
			this.set('settingPassword', false);

		},

		savePassword: function () {

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : SAVE PASSWORD ');

			var data = this.getProperties('password');
			Ember.$.post('/accounts/change_password', data, function(response) {
				if (response.success===true) {
					Ember.Logger.log('~~~~~~~~~ SET PASSWORD SUCCESSS ~~~~~~~~');
					this.set('serverSuccessMessage', 'passsword set');
					this.set('settingPassword', false);
				} else {
					this.set('serverErrorMessage', 'passsword not set, please try again');
				}
			}.bind(this), 'json');

		},


		/* New Addresses */

		addAddress: function(){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : ADD ADDRESS FORM ');
			this.set('addingNewAddress', true);
			this.setProperties({
				'address'				:	'',
				'suite'					: '',
				'city'					: '',
				'state'					: 'WA',
				'country'				: 'US',
				'zip'						: '',
				'address_type'	: 'principal',
				'address_desc'	: ''
			});
		
		},

		saveAddress: function (){

			var data = this.getProperties(
									'address',
									'suite',
									'city',
									'state',
									'country',
									'zip',
									'address_type',
									'address_desc' 
								);
			data['loginToken'] = session.get('loginToken');
			Ember.$.post('/addresses/create', data, function(response) { //addresses_create_ok
				if (response.success===true) {
					Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : SAVED NEW ADDRESS ');
					this.set('addingNewAddress', false);
					this.set('serverSuccessMessage','successfully added new address');
					if(response.address){
						this.get('newAddresses').addObject(Em.Object.create(response.address));
					}
				}
			}.bind(this), 'json');

		},

		cancelAddress: function (){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : CANCEL ADDRESS FORM ');
			this.setProperties({
				'address'				:	'',
				'suite'					: '',
				'city'					: '',
				'state'					: 'WA',
				'country'				: 'US',
				'zip'						: '',
				'address_type'	: 'principal',
				'address_desc'	: ''								
			});
			this.set('addingNewAddress', false);

		},

		removeAddress: function (address){

			this.set('addingNewAddress', false);
			var data = { 'id': address._id };
			Ember.$.post('/addresses/delete', data, function(response) { //addresses/delete
				if (response.success===true) {
					this.get('newAddresses').removeObject(address);
					this.set('serverSuccessMessage', 'successfully removed address');
					Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : DELETE ADDRESS : SUCCESS : ', address);
				}
			}.bind(this), 'json');

		},

		editAddress: function (address){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : EDIT ADDRESS : ', address);
			address.set('isEditing',true);

		},

		doneEditingAddress: function (address){

			var data = Em.Object.create();
					data.setProperties(address);
					data.set('id', address._id);
			Ember.$.post('/addresses/update', JSON.parse(JSON.stringify(data)), function(response) { //addresses_update_ok
				if (response.success===true) {
					Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : DONE EDITING ADDRESS RESPONSE', response);
					address.set('isEditing','');
					this.set('serverSuccessMessage','successfully updated address');
				}
			}.bind(this), 'json');

		},

		/* New Phones */

		addPhone: function(){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : ADD PHONE FORM ');
			this.set('addingNewPhone', true);
			this.setProperties({
				'number'			: '',
				'number_type'	: 'phone',
				'ext'					: '',
				'number_desc'	: ''
			});
		
		},

		savePhone: function (){

			var data = this.getProperties(
									'number',
									'number_type',
									'ext',
									'number_desc'
								);
			data['loginToken'] = session.get('loginToken');
			Ember.$.post('/phone_numbers/create', data, function(response) { //phone_numbers_create_ok
				if (response.success===true) {
					Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : SAVED NEW PHONE NUMBER : SUCCESS');
					this.set('serverSuccessMessage','successfully added new phone');
					this.set('addingNewPhone', false);										
					if(response.phone_number){
						this.get('newPhones').addObject(Em.Object.create(response.phone_number));
					}
				}
			}.bind(this), 'json');

		},

		cancelPhone: function (){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : CANCEL PHONE FORM ');
			this.setProperties({
				'number'			: '',
				'number_type'	: 'phone',
				'ext'					: '',
				'number_desc'	: ''
			});
			this.set('addingNewPhone', false);

		},

		removePhone: function (phone){

			this.set('addingPhone', false);
			var data = { 'id': phone._id };
			Ember.$.post('/phone_numbers/delete', data, function(response) { //phone_numbers_delete_ok
				if (response.success===true) {
					this.get('newPhones').removeObject(phone);
					this.set('serverSuccessMessage', 'successfully removed phone');
					Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : DELETE PHONE : SUCCESS ',phone);
				}
			}.bind(this), 'json');

		},

		editPhoneNumber: function (phone){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : EDIT PHONE : ', phone);
			phone.set('isEditing',true);

		},

		doneEditingPhoneNumber: function (phone){

			var data = Em.Object.create();
					data.setProperties(phone);
					data.set('id', phone._id);
			Ember.$.post('/phone_numbers/update', JSON.parse(JSON.stringify(data)), function(response) { //phone_numbers_update_ok
				if (response.success===true) {
					Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : DONE EDITING PHONE RESPONSE',response);
					phone.set('isEditing','');
					this.set('serverSuccessMessage','successfully updated phone number');
				}
			}.bind(this), 'json');

		},

		/* New Emails */

		addEmail: function(){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : ADD EMAIL FORM ');
			this.set('addingNewEmail', true);
			this.setProperties({
				'email_address'	: '',
				'email_type'		: 'primary',
				'notifications'	: true,
				'email_desc'		: ''
			});
		
		},

		saveEmail: function (){

			var data = this.getProperties(
									'email_address',
									'email_type',
									'notifications',
									'email_desc'
								);
			data['loginToken'] = session.get('loginToken');
			Ember.$.post('/emails/create', data, function(response) { //phone_numbers_create_ok
				if (response.success===true) {
					Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : SAVED NEW EMAIL : SUCCESS');
					this.set('addingNewEmail', false);
					this.set('serverSuccessMessage', 'successfully updated email');
					if(response.email){
						this.get('newEmails').addObject(Em.Object.create(response.email));
					}
				}
			}.bind(this), 'json');

		},

		cancelEmail: function (){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : CANCEL EMAIL FORM ');
			this.setProperties({
				'email_address'	: '',
				'email_type'		: 'primary',
				'notifications'	: true,
				'email_desc'		: ''
			});
			this.set('addingNewEmail', false);

		},

		removeEmail: function (email){

			email.set('addingEmail', false);
			var data = { 'id': email._id };
			Ember.$.post('/emails/delete', data, function(response) { //phone_numbers_delete_ok
				if (response.success===true) {
					
					Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : DELETE EMAIL : SUCCESS | EMAIL -> ', email);
					this.get('newEmails').removeObject(email);
					this.set('serverSuccessMessage', 'successfully removed email');

				}
			}.bind(this), 'json');

		},

		editEmail: function (email){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : EDIT EMAIL : ', email);
			email.set('isEditing', true);

		},

		doneEditingEmail: function (email){

			var data = Em.Object.create();
					data.setProperties(email);
					data.set('id', email._id);
			Ember.$.post('/emails/update', JSON.parse(JSON.stringify(data)), function(response) { //phone_numbers_update_ok
				if (response.success===true) {
					Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : DONE EDITING EMAIL RESPONSE',response);
					email.set('isEditing','');
					this.set('serverSuccessMessage','successfully updated email address');
				}
			}.bind(this), 'json');

		},

		update: function (){

			var data = { 'first_name': this.get('first_name'), 'last_name': this.get('last_name') };
			Ember.$.post('/accounts/add_name', data, function(response) {
				if (response.success===true) {
					Ember.Logger.log('~~~~~~~~ APP -> ROUTES -> ACCOUNT -> DETAILS : FINISHED / ADD NAME : SUCCESS ');
					this.set('serverSuccessMessage','successfully updated account');
				}
			}.bind(this), 'json');

		}

	}

});

