import session from 'appkit/session';
export default Ember.Controller.extend(Ember.Validations.Mixin, {
	
	validations: {
	
		email: {
			format: { with: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/ , message: 'email' }
		},
		
		password: {
			confirmation: true,
			presence: {
				message: ' password',
				if: function(controller) {
					if(controller.get('generatePassword')===false){
						return true;
					}
				}
			}
		},
		
		passwordConfirmation: {
			presence: {
				message: ' confirm password',
				if: function(controller) {
					if(controller.get('generatePassword')===false){
						return true;
					}
				}
			}
		}
	
	},
	
	rememberMe: true,
	generatePassword: false,
	serverErrorMessage: false,

	generatePasswordObserver: function (){
		this.validate().then(function() { Ember.Logger.log('~~~~~~~~~ APP -> CONTROLLER -> REGISTER -> INDEX -> REVALIDATE FOR ACCOUNT PASSWORD'); });
	}.observes('generatePassword'),
	
	actions: {
	
		register: function() {

			var data = this.getProperties('email', 'generatePassword', 'password');
			Ember.$.post('/accounts/register', data, function(response) {
				if (response.success===true) {
					this.send('setSessionCookie', response.login_detail.account_id, response.token, response.account.roles, 'account.register.index');
					this.send('getSessionCookie');
					this.transitionToRoute('account.register.confirmed');
				} else {
					this.set('serverErrorMessage', 'registration not accepted, please try again');
				}

			}.bind(this), 'json');
		
		},	
	},
});