import session from 'appkit/session';
export default Ember.Controller.extend(Ember.Validations.Mixin, {
	validations: {
		password: {
			confirmation: true,
			presence: {
				message: ' password'
			}
		},
		passwordConfirmation: {
			presence: {
				message: ' please confirm password'
			}
		}
	},
	passwordSetMessage: false,
	serverErrorMessage: false,
	actions: {

		/* Reset Password */

		savePassword: function () {

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> PASSWORD :  PASSWORD ');

			var data = this.getProperties('password');
			Ember.$.post('/accounts/change_password', data, function(response) {
				if (response.success===true) {
					Ember.Logger.log('~~~~~~~~~ SET PASSWORD SUCCESSS ~~~~~~~~');
					this.set('serverSuccessMessage', 'passsword succesfully reset, logout and log back in to use your new credentials');
					this.set('settingPassword', false);
				} else {
					this.set('serverErrorMessage', 'passsword not set, please try again');
				}
			}.bind(this), 'json');

		}
	}

});
