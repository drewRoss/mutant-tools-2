import session from 'appkit/session';
export default Ember.Controller.extend(Ember.Validations.Mixin, {
	
	validations: {
	
		password: {
			confirmation: true,
			presence: {
				message: ' password'
			}
		},
	
		passwordConfirmation: {
			presence: {
				message: ' please confirm password'
			}
		}
	},
	
	passwordSetMessage: false,
	
	serverErrorMessage: false,
	
	actions: {

		setPassword: function(setToken) {

			var data = Em.Object.create();
			data.set('password',this.get('password'));
			data.set('reset_token', setToken);

			Ember.Logger.log('~~~~~~~~~ SET PASSWORD WITH TOKEN ~~~~~~~~: ', data);

			Ember.$.post('/accounts/do_password_set', JSON.parse(JSON.stringify(data)), function(response) { //accounts_do_password_set_ok
				if (response.success===true) {
					var setMessage = true;
					this.set('passwordSetMessage',setMessage);
					Ember.Logger.log('~~~~~~~~~ SET PASSWORD SUCCESSS ~~~~~~~~');
				} else {
					this.set('serverErrorMessage', 'passsword not set, please try again');
				}
			}.bind(this), 'json');

		}

	}

});
