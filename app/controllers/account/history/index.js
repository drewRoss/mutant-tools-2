import session from 'appkit/session';
import statics from 'appkit/statics';
export default Ember.Controller.extend({
	
	needs: 'nav',
	session: session,
	accountLogs: [],

	lastLoginMessage: function () {
	
		var loginLastIP = this.get('session.loginLastIP'),
				loginLast = this.get('session.loginLast');
		if ( loginLastIP && loginLast ) {
			return 'Last-login from ip: [' + loginLastIP + '] on [' + loginLast + '].';
		}
	
	}.property('session.loginLastIP', 'session.loginLast'),


	getAccountHistory: function(){

		Ember.$.getJSON('/logs', function(response) { //logs_ok
			if (response) {
				response.reverseObjects();
				response.forEach(function(res) {
					res.mo_date = moment(res.created_at).format('MMMM Do YYYY, h:mm:ss a');
					this.get('accountLogs').addObject(Em.Object.create(res));
				}.bind(this));
				Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> HOME : GET ACCOUNT LOGS SUCCESS ~~~~~~~~',this.get('accountLogs'));
			}
		}.bind(this), 'json');

	}.on('init')
	
});

