import controls from 'appkit/controls';
import statics from 'appkit/statics';
export default Ember.Controller.extend(Ember.Validations.Mixin, {
	
	validations: {

		test: {
			presence: {
				message: ' test is whatever'
			}
		},

		email: {
			presence: {
				message: ' email is whatever'
			}
		},

		password: {
			confirmation: true,
			presence: {
				message: ' password'
			}
		},
		
		passwordConfirmation: {
			presence: {
				message: ' please confirm password'
			}
		}

	},

	controls: controls,
	statics: statics,
	revalidate: true,

	/*

		WANT TO DYNAMICALLY GENERATE VALIDATION OBJECT - VALIDATIONS JSON OBJECT WILL
		COME FROM CONTROLS.JS WHICH REPRESENTS AN API OF ALL FIELDS AND VALIDATIONS
		HBS WILL CREATE THE CONTROLS BY ITERATING OVER CONTROLS OBJECT. JS METHOD WILL
		ADD VALIDATION FOR EACH BY BINDING IT'S VALIDATION AND REVALIDATION TO EACH 
		FIELD.

		STEP 1. VALIDATE TEST FILED BY PASSING OBJECT TO VALIDATE() METHOD ON INIT

	*/

	checkFields: function (){

		//{ dynfield: { presence: { message: ' dynfield is whatever' } } }
		//this.validations.pushObject(findValidator(validator).create(what));

		this.validations["dynfield"] = { presence: { message: ' dynfield is whatever' } } ;

	}.on('init'),

	revalidateObserver: function (){

		this.validate().then(function() {

			Ember.Logger.log('~~~~~~~~~ APP -> CONTROLLER -> HOME -> REVALIDATE FOR ACCOUNT PASSWORD', this.validations);

		});

	}.observes('revalidate'),

	actions : {

		/* Re-Validate-All */

		validateFields: function (){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> ACCOUNT -> DETAILS : SET PASSWORD FORM ');
			this.set('settingPassword', true);

		}

	}

});
