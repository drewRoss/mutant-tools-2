export default Ember.Controller.extend(Ember.Validations.Mixin, {
	validations: {
		email: {
			presence: { message: 'email (required)' },
			format: { with: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/ , message: ' format: me@email.com' }
		}
	},
	passwordSetMessage: false,
	serverErrorMessage: false,
	actions: {
		recoverPassword: function() {
			var data = this.getProperties('email');
			Ember.$.post('/accounts/set_password', data, function(response) { //accounts_set_password_ok
				if (response.success===true) {
					var setMessage = 'an email with password recovery link has been sent to <' + this.get('email') + '>. please set your password within the hour.';
					this.set('passwordSetMessage',setMessage);
					Ember.Logger.log('~~~~~~~~~ RECOVER PASSWORD SUCCESSS ~~~~~~~~');
				} else {
					this.set('serverErrorMessage', 'passsword not recovered, please try again');
				}
			}.bind(this), 'json');
		}
	},
	cancelPasswordRecovery: function(){
		this.transitionToRoute('welcome');
	}
});