import session from 'appkit/session';
export default Ember.Controller.extend(Ember.Validations.Mixin, {
	validations: {
		password: {
			confirmation: true,
			presence: {
				message: ' password'
			}
		},
		passwordConfirmation: {
			presence: {
				message: ' please confirm password'
			}
		}
	},
	passwordSetMessage: false,
	serverErrorMessage: false,
	actions: {

		setPassword: function(setToken) {
			
			Ember.Logger.log('~~~~~~~~~ SET PASSWORD WITH TOKEN ~~~~~~~~: ' + setToken);

			var data = this.getProperties('password');
			Ember.$.post('/accounts/do_password_set', data, function(response) { //accounts_do_password_set_ok
				if (response.success===true) {
					
					var setMessage = true;
					this.set('passwordSetMessage',setMessage);
					Ember.Logger.log('~~~~~~~~~ SET PASSWORD SUCCESSS ~~~~~~~~');

				} else {
					this.set('serverErrorMessage', 'passsword not set, please try again');
				}
			}.bind(this), 'json');

		}
	}
});
