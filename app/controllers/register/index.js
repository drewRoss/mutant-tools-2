import session from 'appkit/session';
export default Ember.Controller.extend(Ember.Validations.Mixin, {
	validations: {
		email: {
			presence: { message: 'email (required)' },
			format: { with: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/ , message: ' format: me@email.com' }
		},
		password: {
			confirmation: true,
			presence: {
				message: ' password',
				if: function(controller) {
					if(controller.get('generatePassword')===false){
						return true;
					}
				}
			}
		},
		passwordConfirmation: {
			presence: {
				message: ' please confirm password',
				if: function(controller) {
					if(controller.get('generatePassword')===false){
						return true;
					}
				}
			}
		}
	},
	rememberMe: true,
	generatePassword: true,
	serverErrorMessage: false,

	generatePasswordObserver: function (){
		this.validate().then(function() { Ember.Logger.log('~~~~~~~~~ APP -> CONTROLLER -> REGISTER -> INDEX -> REVALIDATE FOR ACCOUNT PASSWORD'); });
	}.observes('generatePassword'),
	
	actions: {
	
		register: function() {

			var data = this.getProperties('email', 'generatePassword', 'password');
			Ember.$.post('/accounts/register', data, function(response) {
				if (response.success===true) {
					this.send('setSessionCookie', response.login_detail.account_id, response.token, response.account.roles, 'register.index');
					this.send('getSessionCookie');
					this.transitionToRoute('register.confirmed');
				} else {
					this.set('serverErrorMessage', 'registration not accepted, please try again');
				}

			}.bind(this), 'json');
		
		},	
	},
});