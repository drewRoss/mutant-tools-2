import session from 'appkit/session';
import statics from 'appkit/statics';
export default Ember.Controller.extend({

	statics: statics,
	internationalAddress: false,
	addingNewAddress: false,
	addingNewPhone: false,
	addingNewEmail: false,
	readyToSave: Ember.computed.or('addingNewAddress', 'addingNewPhone', 'addingNewEmail'),
	newAddresses: [],
	newPhones: [],
	newEmails: [],

	internationalAddressObserver: function () {

		if(this.get('internationalAddress')===true){
			this.set('state','');
			this.set('country','CA');
		} else {
			this.set('state','WA');
			this.set('country','US');
		}

	}.observes('internationalAddress'),

	getAccountDetails: function(){

		Ember.$.getJSON('/account/details', function(response) { //account_details_ok
			if (response) {
				Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> REGISTER -> CONFIRMED : GET ACCOUNT DETAILS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ DETAILS DETAILS DETAILS',response);
				this.set('first_name', response.first_name);
				this.set('last_name', response.last_name);
				if (response.addresses) {
					response.addresses.forEach(function(address) {
						this.get('newAddresses').addObject(Em.Object.create(address));
					}.bind(this));
				}
				if (response.phone_numbers) {
					response.phone_numbers.forEach(function(number) {
						this.get('newPhones').addObject(Em.Object.create(number));
					}.bind(this));
				}
				if (response.emails) {
					response.emails.forEach(function(email) {
						this.get('newEmails').addObject(Em.Object.create(email));
					}.bind(this));
				}
			}
		}.bind(this), 'json');

	}.on('init'),
	
	actions : {

		/* New Addresses */

		addAddress: function(){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> REGISTER -> CONFIRMED : ADD ADDRESS FORM ');
			this.set('addingNewAddress', true);
			this.setProperties({
				'address'				:	'',
				'suite'					: '',
				'city'					: '',
				'state'					: 'WA',
				'country'				: 'US',
				'zip'						: '',
				'address_type'	: 'principal',
				'address_desc'	: ''
			});
		
		},

		saveAddress: function (){

			this.set('addingNewAddress', false);
			var data = this.getProperties(
									'address',
									'suite', 
									'city',
									'state',
									'country',
									'zip',
									'address_type',
									'address_desc' 
								);
			data['loginToken'] = session.get('loginToken');
			Ember.$.post('/addresses/create', data, function(response) { //addresses_create_ok
				if (response.success===true) {
					Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> REGISTER -> CONFIRMED : SAVED NEW ADDRESS ');
					if(response.address){
						this.get('newAddresses').addObject(Em.Object.create(response.address));
					}
				}
			}.bind(this), 'json');

		},

		cancelAddress: function (){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> REGISTER -> CONFIRMED : CANCEL ADDRESS FORM ');
			this.setProperties({
				'address'				:	'',
				'suite'					: '',
				'city'					: '',
				'state'					: 'WA',
				'country'				: 'US',
				'zip'						: '',
				'address_type'	: 'principal',
				'address_desc'	: ''								
			});
			this.set('addingNewAddress', false);

		},

		removeAddress: function (address){

			this.set('addingNewAddress', false);
			var data = { 'id': address._id };
			Ember.$.post('/addresses/delete', data, function(response) { //addresses/delete
				if (response.success===true) {
					this.get('newAddresses').removeObject(address);
					Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> REGISTER -> CONFIRMED : DELETE ADDRESS : SUCCESS : ', address);
				}
			}.bind(this), 'json');

		},

		editAddress: function (address){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> REGISTER -> CONFIRMED : EDIT ADDRESS : ', address);
			address.set('isEditing',true);

		},

		doneEditingAddress: function (address){

			var data = Em.Object.create();
					data.setProperties(address);
					data.set('id', address._id);
			address.set('isEditing','');
			Ember.$.post('/addresses/update', JSON.parse(JSON.stringify(data)), function(response) { //addresses_update_ok
				if (response.success===true) {
					Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> REGISTER -> CONFIRMED : DONE EDITING ADDRESS RESPONSE', response);
				}
			}.bind(this), 'json');

		},

		/* New Phones */

		addPhone: function(){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> REGISTER -> CONFIRMED : ADD PHONE FORM ');
			this.set('addingNewPhone', true);
			this.setProperties({
				'number'			: '',
				'number_type'	: 'phone',
				'ext'					: '',
				'number_desc'	: ''
			});
		
		},

		savePhone: function (){

			this.set('addingNewPhone', false);
			var data = this.getProperties(
									'number',
									'number_type',
									'ext',
									'number_desc'
								);
			data['loginToken'] = session.get('loginToken');
			Ember.$.post('/phone_numbers/create', data, function(response) { //phone_numbers_create_ok
				if (response.success===true) {
					Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> REGISTER -> CONFIRMED : SAVED NEW PHONE NUMBER : SUCCESS');
					if(response.phone_number){
						this.get('newPhones').addObject(Em.Object.create(response.phone_number));
					}
				}
			}.bind(this), 'json');

		},

		cancelPhone: function (){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> REGISTER -> CONFIRMED : CANCEL PHONE FORM ');
			this.setProperties({
				'number'			: '',
				'number_type'	: 'phone',
				'ext'					: '',
				'number_desc'	: ''
			});
			this.set('addingNewPhone', false);

		},

		removePhone: function (phone){

			this.set('addingPhone', false);
			var data = { 'id': phone._id };
			Ember.$.post('/phone_numbers/delete', data, function(response) { //phone_numbers_delete_ok
				if (response.success===true) {
					this.get('newPhones').removeObject(phone);
					Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> REGISTER -> CONFIRMED : DELETE PHONE : SUCCESS ',phone);
				}
			}.bind(this), 'json');

		},

		editPhoneNumber: function (phone){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> REGISTER -> CONFIRMED : EDIT PHONE : ', phone);
			phone.set('isEditing',true);

		},

		doneEditingPhoneNumber: function (phone){

			var data = Em.Object.create();
					data.setProperties(phone);
					data.set('id', phone._id);
					phone.set('isEditing','');
			Ember.$.post('/phone_numbers/update', JSON.parse(JSON.stringify(phone)), function(response) { //phone_numbers_update_ok
				if (response.success===true) {
					Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> REGISTER -> CONFIRMED : DONE EDITING PHONE RESPONSE',response);
				}
			}.bind(this), 'json');

		},

		/* New Emails */

		addEmail: function(){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> REGISTER -> CONFIRMED : ADD EMAIL FORM ');
			this.set('addingNewEmail', true);
			this.setProperties({
				'email_address'	: '',
				'email_type'		: 'primary',
				'notifications'	: false,
				'email_desc'		: ''
			});
		
		},

		saveEmail: function (){

			this.set('addingNewEmail', false);
			var data = this.getProperties(
									'email_address',
									'email_type',
									'notifications',
									'email_desc'
								);
			data['loginToken'] = session.get('loginToken');
			Ember.$.post('/emails/create', data, function(response) { //phone_numbers_create_ok
				if (response.success===true) {
					Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> REGISTER -> CONFIRMED : SAVED NEW EMAIL : SUCCESS');
					if(response.email){
						this.get('newEmails').addObject(Em.Object.create(response.email));
					}
				}
			}.bind(this), 'json');

		},

		cancelEmail: function (){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> REGISTER -> CONFIRMED : CANCEL EMAIL FORM ');
			this.setProperties({
				'email_address'	: '',
				'email_type'		: 'primary',
				'notifications'	: false,
				'email_desc'		: ''
			});
			this.set('addingNewEmail', false);

		},

		removeEmail: function (email){

			email.set('addingEmail', false);
			var data = { 'id': email._id };
			Ember.$.post('/emails/delete', data, function(response) { //phone_numbers_delete_ok
				if (response.success===true) {
					
					Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> REGISTER -> CONFIRMED : DELETE EMAIL : SUCCESS | EMAIL -> ', email);
					this.get('newEmails').removeObject(email);

				}
			}.bind(this), 'json');

		},

		editEmail: function (email){

			Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> REGISTER -> CONFIRMED : EDIT EMAIL : ', email);
			email.set('isEditing', true);

		},

		doneEditingEmail: function (email){

			var data = Em.Object.create();
					data.setProperties(email);
					data.set('id', email._id);
			email.set('isEditing','');
			Ember.$.post('/emails/update', JSON.parse(JSON.stringify(email)), function(response) { //phone_numbers_update_ok
				if (response.success===true) {
					Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> REGISTER -> CONFIRMED : DONE EDITING EMAIL RESPONSE',response);
				}
			}.bind(this), 'json');

		},

		finished: function (){

			var data = { 'first_name': this.get('first_name'), 'last_name': this.get('last_name'), 'loginToken': session.get('loginToken') };
			Ember.$.post('/accounts/add_name', data, function(response) { //accounts_add_name_ok
				if (response.success===true) {
					Ember.Logger.log('~~~~~~~~ APP -> CONTROLLERS -> REGISTER -> CONFIRMED : FINISHED / ADD FIRST, LAST NAME : SUCCESS ');
				}
			}.bind(this), 'json');
			this.transitionToRoute('home');

		},

	}

});

